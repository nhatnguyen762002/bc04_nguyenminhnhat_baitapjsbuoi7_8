var numArr = [];
var numArr2 = [];

function timDuong() {
  return numArr.filter(function (number) {
    return number > 0;
  });
}

function timAm() {
  return numArr.filter(function (number) {
    return number < 0;
  });
}

function timChan() {
  return numArr.filter(function (number) {
    return number % 2 == 0;
  });
}

function tinhTongDuong(mangDuong) {
  return mangDuong().reduce(function (total, number) {
    return total + number;
  }, 0);
}

function timNhoNhat() {
  var min = numArr[0];
  numArr.forEach(function (number) {
    if (min > number) {
      min = number;
    }
  });
  return min;
}

function timDuongNhoNhat(mangDuong) {
  var min = mangDuong()[0];
  mangDuong().forEach(function (number) {
    if (min > number) {
      min = number;
    }
  });
  return min;
}

function timChanCuoi(mangChan) {
  var chanCuoi = -1;
  mangChan().forEach(function (number) {
    chanCuoi = number;
  });
  return chanCuoi;
}

function sapXepTangDan() {
  var mangTangDan = Array.from(numArr);
  var tangDanEl = document.querySelector("#txt-tang-dan");

  mangTangDan.sort(function (a, b) {
    return a - b;
  });

  tangDanEl.innerText = `${mangTangDan.join(", ")}`;
}

function kiemTraSNT(number) {
  if (number < 2) {
    return 0;
  }

  for (var i = 2; i < number; i++) {
    if (number % i == 0) {
      return 0;
    }
  }
  return 1;
}

function themSo() {
  var numValue = document.querySelector("#txt-number").value * 1;
  document.querySelector("#txt-number").value = "";

  var mangEl = document.querySelector("#txt-mang");
  var ketQuaEl = document.querySelector("#txt-ket-qua");

  numArr.push(numValue);
  mangEl.innerText = numArr.join(", ");

  ketQuaEl.innerText = `1. Tổng số dương: ${tinhTongDuong(timDuong)}
  2. Đếm số dương: ${timDuong().length}
  3. Số nhỏ nhất: ${timNhoNhat()}
  4. Số dương nhỏ nhất: ${timDuongNhoNhat(timDuong)}
  5. Số chẵn cuối cùng: ${timChanCuoi(timChan)}`;

  sapXepTangDan();

  // SNT Đầu
  var sntDau = -1;
  for (i = 0; i < numArr.length; i++) {
    if (kiemTraSNT(numArr[i]) == 1) {
      sntDau = numArr[i];
      break;
    }
  }
  document.querySelector("#txt-nguyen-to").innerText = sntDau;

  // So sánh âm dương
  if (timDuong().length > timAm().length) {
    document.querySelector("#txt-ss").innerText = `Số âm ít hơn số dương`;
  } else if (timDuong().length == timAm().length) {
    document.querySelector("#txt-ss").innerText = `Số âm bằng số dương`;
  } else {
    document.querySelector("#txt-ss").innerText = `Số âm nhiều hơn số dương`;
  }
}

function doiCho() {
  var so1Value = document.querySelector("#txt-so-1").value * 1;
  var so2Value = document.querySelector("#txt-so-2").value * 1;
  var doiChoEl = document.querySelector("#txt-doi-cho");
  var mangDoiCho = Array.from(numArr);

  var temp = mangDoiCho[so1Value];
  mangDoiCho[so1Value] = mangDoiCho[so2Value];
  mangDoiCho[so2Value] = temp;

  doiChoEl.innerText = `${mangDoiCho.join(", ")}`;
}

function themSo2() {
  var num2Value = document.querySelector("#txt-number2").value * 1;
  document.querySelector("#txt-number2").value = "";

  var mang2El = document.querySelector("#txt-mang2");

  numArr2.push(num2Value);
  mang2El.innerText = numArr2.join(", ");

  var tongNguyenEl = document.querySelector("#txt-tong-nguyen");
  var tongNguyen = numArr2.filter(function (number) {
    return Number.isInteger(number) === true;
  });
  tongNguyenEl.innerText = `Có ${tongNguyen.length} số nguyên`;
}
